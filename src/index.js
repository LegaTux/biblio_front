import React from 'react';
import ReactDOM from 'react-dom';
// import Router fro 'react-router';
import App from './App';
import '../styles/index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
